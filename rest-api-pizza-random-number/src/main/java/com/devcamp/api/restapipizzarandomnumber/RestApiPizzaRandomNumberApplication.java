package com.devcamp.api.restapipizzarandomnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiPizzaRandomNumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiPizzaRandomNumberApplication.class, args);
	}

}
