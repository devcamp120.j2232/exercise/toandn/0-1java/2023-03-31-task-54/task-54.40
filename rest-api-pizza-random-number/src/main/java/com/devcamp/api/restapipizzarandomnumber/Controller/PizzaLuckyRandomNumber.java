package com.devcamp.api.restapipizzarandomnumber.Controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")

public class PizzaLuckyRandomNumber {
  @CrossOrigin
  @GetMapping("/devcamp-date")
  public String getDateViet(String title) {
    DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
    LocalDate today = LocalDate.now(ZoneId.systemDefault());
    return String.format("Hello %s ! Hôm nay %s, mua 1 tặng 1.", title, dtfVietnam.format(today));
  }

  @CrossOrigin
  @GetMapping("/devcamp-meassage")
  public String getInforNameNumber(String username, String firstname, String lastname) {
    String meassage = "";
    int randomIntNumber = 1 + (int) (Math.random() * (6 - 1));
    meassage = "Xin chào: " + username + ", Số may mắn hôm nay của bạn là: " + randomIntNumber;
    return meassage;
  }

}
